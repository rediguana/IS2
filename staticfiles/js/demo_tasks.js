$(function(){

    var us;

    var number = document.getElementById('horas');
    number.onkeydown = function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
                || (e.keyCode > 47 && e.keyCode < 58)
                || e.keyCode == 8)) {
            return false;
        }
    }

    number.onkeydown = function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
                || (e.keyCode > 45 && e.keyCode < 58)
                || e.keyCode == 8)) {
            return false;
        }
    }

    $( "#dialog" ).dialog(
        {
            autoOpen: false,
            height: 200,
            width: 300,
            modal: true,
            open: function(event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
            buttons: {
                "Ok": func,

            }
        }
    );

    function func() {
        var horas = $("#horas").val();

        $.ajax({
            url: 'updateHoras',
            data: {
                'horas':horas,
                'us':us
            },
            dataType:'json',
            success: function(data) {
                if (!data.ok){
                    alert("No pertenece al flujo")

                }
                else {
                }
            }

        } )
        $( this ).dialog( "close" );
        number.value = 0


    }

    var tasks = function(){

        $("#add_new_task").on("click",function(){
            var nt = $("#new_task").val();
            if(nt != ''){

                var task = '<div class="task-item task-primary">'
                    +'<div class="task-text">'+nt+'</div>'
                    +'<div class="task-footer">'
                    +'<div class="pull-left"><span class="fa fa-clock-o"></span> now</div>'
                    +'</div>'
                    +'</div>';

                $("#tasks").prepend(task);
            }
        });

        $("#tasks,#tasks_progreess,#tasks_completed").sortable({
            items: "> .misuserstories",
            connectWith: "#tasks_progreess,#tasks_completed, #tasks",
            handle: ".task-text",
            // cancel: ".us",
            receive: function(event, ui) {
                var estado;
                if(this.id == "tasks_completed"){
                    estado = 'Done';
                }
                if(this.id == "tasks_progreess"){
                    estado = 'Doing';
                }
                if(this.id == "tasks"){
                    estado = 'To do';
                }

                var fase = this.parentNode.parentNode.parentNode.id;

                // alert(this.innerHTML)
                // us = ui.item.context.id.split("_")[1]
                us = ui.item.attr("id").split("_")[1];


                var faseanterior = ui.sender.parent().parent().parent().attr("id");
                var estadoanterior = ui.sender.attr("id");

                if(estadoanterior == "tasks_completed"){
                    estadoanterior = 'Done';
                }
                if(estadoanterior == "tasks_progreess"){
                    estadoanterior = 'Doing';
                }
                if(estadoanterior == "tasks"){
                    estadoanterior = 'To do';
                }

                datos = {
                    'fase':fase,
                    'estado':estado,
                    'faseanterior':faseanterior,
                    'estadoanterior':estadoanterior,
                    'us':us
                }



                $.ajax({
                        url: 'update',
                        data: datos,
                        dataType:'json',
                        success: function(data) {
                            if (!data.ok){
                                alert("No pertenece al flujo")
                                $(ui.sender).sortable('cancel');

                            }
                            else if(!data.oksecuencial){
                                alert("Por favor, avance secuencialmente"),
                                    $(ui.sender).sortable('cancel');
                            }
                            else {
                                $("#dialog").dialog("open");
                            }
                        },

                    }

                );

                page_content_onresize();
            }
        }).disableSelection();

    }();

});
