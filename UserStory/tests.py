from django.test import TestCase
from django.test import TestCase
from django.urls import reverse
from Accounts.models import User, Rol
from Proyecto.models import Proyecto
from team.models import team
from UserStory.models import UserStory,UserStoryHistory
from guardian.shortcuts import assign_perm
from django.contrib import auth

# Create your tests here.
class USCreateViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_agregar_nuevo_us(self):
        """
        Setup inicial del test y luego se prueba agregar un User Story, a un proyecto ya existente para esto se le brinda,
        al usuario los permisos adecuados(modificar_proyecto y crear_us) mediante una llamada post y de parametro el
        id del proyecto
        """
        self.login()
        usuario = auth.get_user(self.client)



        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('UserStory.crear_us', rol)
        assign_perm('UserStory.listar_us', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06', fechainicio='2018-04-06')

        member = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)

        response = self.client.post(reverse('Proyecto:UserStory:crearUs', kwargs={'pk':proyecto.id}), data={
            'titulo': 'Login', 'descripcion_corta': 'Entrada al sistema', 'urgencia': 3, 'valor_de_negocio': 4,
            'valor_tecnico': 5, 'tiempo_estimado_de_terminacion': 2, 'estado': 1}, follow=True)

        self.assertContains(response, 'Login', msg_prefix="Error en UserStory/Test_agregar_nuevo_UserStory" )

    def test_agregar_nuevo_us_sin_permiso(self):
        """
        Setup inicial y luego se procede a intentar agregar un User Story a un proyecto ya existente, se le brinda
        al usuario el permiso de modificar_proyecto sin embargo no el de crear_us por lo tanto la llamada post retorna
        un codigo http 403
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)

        #assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        #assign_perm('UserStory.crear_us', rol)
        assign_perm('UserStory.listar_us', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        member = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)

        response = self.client.post(reverse('Proyecto:UserStory:crearUs', kwargs={'pk': proyecto.id}), data={
            'titulo': 'Login', 'descripcion_corta': 'Entrada al sistema', 'urgencia': 3, 'valor_de_negocio': 4,
            'valor_tecnico': 5, 'tiempo_estimado_de_terminacion': 2, 'estado': 1}, follow=True)

        self.assertEqual(response.status_code, 403, msg="Error en UserStory/Test_agregar_nuevo_UserStory_sin_permiso")

class Modificar_User_StoryTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
    def test_modificar_us(self):
        """
        Se ejecuta el setup inicial del test y luego se prueba modificar un User Story de un proyecto ya existente
        para esto se le brinda, al usuario los permisos adecuados(modificar_proyecto y modificar_us) mediante una
        llamada post y de parametro el id del proyecto, el campo a ser modificado, y nos aseguramos que el dato ha
        sido modificado como se espera con assertContains
        """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('UserStory.modificar_us', rol)
        assign_perm('UserStory.listar_us', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                      urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                      tiempo_estimado_de_terminacion=2).id

        response = self.client.post(reverse('Proyecto:UserStory:modificarUs', kwargs={'pk': proyecto.id, 'pku': us}), data={
            'titulo': 'Menu'}, follow=True)

        self.assertContains(response, 'Menu', msg_prefix="Error en UserStory/Test_Modificar_UserStory")

    def test_modificar_us_sin_permiso(self):
        """
       Setup inicial y luego se procede a intentar modifcar un User Story  de un proyecto ya existente, se le brinda
       al usuario el permiso de modificar_proyecto sin embargo no el de modificar_us por lo tanto la llamada post
       retorna un codigo http 403
       :return:
       """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        # assign_perm('UserStory.modificar_us', rol)
        assign_perm('UserStory.listar_us', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                      urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                      tiempo_estimado_de_terminacion=2).id

        response = self.client.post(reverse('Proyecto:UserStory:modificarUs', kwargs={'pk': proyecto.id, 'pku': us}),
                                    data={'titulo': 'Menu'}, follow=True)

        self.assertEqual(response.status_code, 403, msg="Error en UserStory/Test_eliminar_us_sin_permiso")


class Eliminar_User_StoryTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
    def test_eliminar_us(self):
        """
        Setup inicial del test y luego se prueba eliminar un User Story de un proyecto ya existente para esto
        se le brinda,al usuario los permisos adecuados(modificar_proyecto y eliminar_us) mediante una llamada post
        y de parametro el id del proyecto, el id del User Story a ser borrado, y nos aseguramos que el miembro ha
        sido borrado como se espera con assertContains
        """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('UserStory.eliminar_us', rol)
        assign_perm('UserStory.listar_us', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                      urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                      tiempo_estimado_de_terminacion=2).id

        response = self.client.post(reverse('Proyecto:UserStory:eliminarUs', kwargs={'pk': proyecto.id, 'pku': us}),
                                    follow=True)

        self.assertNotContains(response, 'Login', msg_prefix="Error en UserStory/Test_Eliminar_UserStory")
    def test_eliminar_us_sin_permiso(self):
        """
        Setup inicial y luego se procede a intentar eliminar un User Story de un proyecto ya existente, se le brinda
        al usuario el permiso de modifiar_proyecto sin embargo no el de eliminar_us por lo tanto la llamada post retorna
        un codigo http 403
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        #assign_perm('UserStory.eliminar_us', rol)
        assign_perm('UserStory.listar_us', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                      urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                      tiempo_estimado_de_terminacion=2).id

        response = self.client.post(reverse('Proyecto:UserStory:eliminarUs', kwargs={'pk': proyecto.id, 'pku': us}),
                                    follow=True)
        self.assertEqual(response.status_code, 403, msg="Error en UserStory/Test_eliminar_us_sin_permiso")

# class Redefinir_User_StoryTests(TestCase):
#     def login(self):
#         usuario = User(username='matias', email="abc@gmail.com")
#         usuario.set_password('123456')
#         usuario.save()
#
#         self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
#     def test_redefinir_us(self):
#         """
#         Setup inicial del test y luego se prueba agregar un miembro, a un proyecto ya existente para esto se le brinda,
#         al usuario los permisos adecuados(modificar_proyecto y crear_team) mediante una llamada post y de parametro el
#         id del proyecto
#         """
#         self.login()
#         usuario = auth.get_user(self.client)
#
#         assign_perm('Proyecto.modificar_proyecto', usuario)
#         rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
#         assign_perm('UserStory.redefinir_us', rol)
#         assign_perm('UserStory.listar_us', rol)
#         assign_perm('UserStory.listar_us_history', rol)
#
#         proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
#                                            fechainicio='2018-04-06')
#
#         team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
#         us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
#                                  urgencia=3, valor_de_negocio=5, valor_tecnico=10, flujo='Desarrollo',
#                                  tiempo_estimado_de_terminacion=2).id
#
#
#         response = self.client.post(reverse('Proyecto:UserStory:redefinirUs', kwargs={'pk': proyecto.id, 'pku': us}), data={
#             'titulo': 'Menu'}, follow=True)
#
#
#         self.assertContains(response, 'Menu', msg_prefix="Error en team/Test_Redefinir_UserStory")
#         self.assertContains(response, 10, msg_prefix="Error en team/Test_Redefinir_UserStory")
#
#         #Se verifica que se creo una copia del objeto inicial para el historial
#
#         response = self.client.get(reverse('Proyecto:UserStory:historialUs',  kwargs={'pk': proyecto.id, 'pku': us}))
#
#         self.assertContains(response, "Login")
#
#     def test_redefinir_us_sin_permiso(self):
#         """
#         Setup inicial del test y luego se prueba agregar un miembro, a un proyecto ya existente para esto se le brinda,
#         al usuario los permisos adecuados(modificar_proyecto y crear_team) mediante una llamada post y de parametro el
#         id del proyecto
#         """
#         self.login()
#         usuario = auth.get_user(self.client)
#
#         assign_perm('Proyecto.modificar_proyecto', usuario)
#         rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
#        # assign_perm('UserStory.modificar_us', rol)
#         assign_perm('UserStory.listar_us', rol)
#
#         proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
#                                            fechainicio='2018-04-06')
#
#         team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
#         us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
#                                       urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
#                                       tiempo_estimado_de_terminacion=2).id
#
#         response = self.client.post(reverse('Proyecto:UserStory:redefinirUs', kwargs={'pk': proyecto.id, 'pku': us}),
#                                     data={
#                                         'titulo': 'Menu'}, follow=True)
#
#         self.assertEqual(response.status_code, 403, msg="Error en team/Test_redefinir_us_sin_permiso")