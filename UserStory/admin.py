from django.contrib import admin

from .models import UserStory, UserStoryHistory
# Register your models here.

admin.site.register(UserStory)
admin.site.register(UserStoryHistory)
