from Accounts.models import User
from team.models import team
from django.contrib.auth import models
from team.models import team
from Proyecto.models import Proyecto

class CustomBackend:
    def authenticate(self, request, username=None, password=None):
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    def has_perm(self, user_obj, perm, obj=None):
        u = User.objects.get(username=user_obj)
        app_label, perm = perm.split('.')
        if isinstance(obj,Proyecto):
            try:
                t = u.team_set.get(proyecto=obj)
                # permisos = t.rol.permissions.get(codename-)
                # if perm in t.rol.permissions.all():
                #     return True
                if t.rol.permissions.get(codename=perm):
                    return True

            except team.DoesNotExist:
                # print('No sos parte del team!')
                return False
            except models.Permission.DoesNotExist:
                # print('Sin permiso')
                return False


        return False

    def get_perms(self, obj, user):
        # print('mi backend')
        if user.is_superuser:
            return user.get_all_permissions()
        if isinstance(obj,Proyecto):
            try:
                t = user.team_set.get(proyecto=obj)
                permObj = t.rol.permissions.all()
                perms = []
                for p in permObj:
                    perms.append(p.content_type.app_label + '.' + p.codename)
                return perms;

            except team.DoesNotExist:
                # print('No sos parte del team!')
                return False
        return False
