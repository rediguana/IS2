from django.db import models
from django.contrib.auth.models import Group
from UserStory.models import UserStory
from auditlog.registry import auditlog

# Create your models here.
class Daily(models.Model):
    us = models.ForeignKey(UserStory, on_delete=models.DO_NOTHING)
    texto = models.CharField(max_length=200)
    fecha = models.DateTimeField(auto_now_add=True)
    autor = models.CharField(max_length=80, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    is_registro = models.BooleanField(default=False)
    horas_trabajadas = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)

    def __str__(self):
        return str(self.id)

    class Meta():
        default_permissions = []
        permissions = (
            ("crear_daily", "Crear Dailies"),
            ("listar_daily", "Ver lista de Dailies"),
            ("modificar_daily", "Modificar Dailies"),
            ("eliminar_daily", "Eliminar Dailies"),
        )

auditlog.register(Daily)