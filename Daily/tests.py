from django.test import TestCase
from django.urls import reverse
from Accounts.models import User, Rol
from Proyecto.models import Proyecto
from Daily.models import Daily
from team.models import team
from UserStory.models import UserStory,UserStoryHistory
from guardian.shortcuts import assign_perm
from django.contrib import auth

# Create your tests here.


class CrearDaily(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
    def test_crear_daily(self):
        """
        Se ejecuta el setup inicial del test y luego se prueba crear un Daily perteneciente a un UserStory ya
        existente para esto se le brinda, al usuario los permisos adecuados(crear_daily y listar_daily) y
        mediante una llamada post con el id del proyecto, del UserStory creamos el objeto, nos aseguramos que
        la operacion fue exitosa con AssertContains
        """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('Daily.crear_daily', rol)
        assign_perm('Daily.listar_daily', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')


        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                 urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                 tiempo_estimado_de_terminacion=2).id

        response = self.client.post(reverse('Proyecto:UserStory:Daily:CrearDaily',
                                            kwargs={'pk': proyecto.id, 'pku': us}),
                                    data={'texto': 'Avance general del 50%', 'us': us, 'autor': 'jorge'}, follow=True)
        self.assertContains(response, 'general', msg_prefix="Error en Daily/test_crear_daily")

    def test_crear_daily_sin_permiso(self):
        """
       Setup inicial y luego se procede a intentarcrear un Daily  perteneciente a un UserStory ya existente, pero
        no se le brinda al usuario el permiso  crear_daily por lo tanto la llamada post
       retorna un codigo http 403
       :return:
       """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('Daily.crear_daily', rol)
       # assign_perm('Daily.modificar_daily', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                      urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                      tiempo_estimado_de_terminacion=2).id

        response = self.client.post(reverse('Proyecto:UserStory:Daily:CrearDaily', kwargs={'pk': proyecto.id, 'pku': us})
                                    , data={'texto': 'Avance general del 50%', 'us': us, 'autor': 'jorge'}, follow=True)

        self.assertEqual(response.status_code, 403, msg="Error en Daily/test_crear_daily_sin_permiso")


class ModificarDaily(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
    def test_modificar_daily(self):
        """
        Se ejecuta el setup inicial del test y luego se prueba modificar un Daily perteneciente a un User Story
        ya existente para esto se le brinda, al usuario los permisos adecuados(modificar_proyecto y modificar_daily)
        mediante una llamada post y de parametro el id del proyecto, el id del UserStory , el id del Daily y
        el campo a ser modificado, y nos aseguramos que el dato ha sido modificado como se espera con assertContains
        """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('Daily.modificar_daily', rol)
        assign_perm('Daily.listar_daily', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                 urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                 tiempo_estimado_de_terminacion=2)

        d = Daily.objects.create(texto="Avance del 45%", us=us, autor='Jorge').id
        response = self.client.post(reverse('Proyecto:UserStory:Daily:ModificarDaily',
                                            kwargs={'pk': proyecto.id, 'pku': us.id, 'pkd': d}), data={'texto':'Sin avances'})
        response = self.client.get(reverse('Proyecto:UserStory:Daily:ListarDailies',
                                            kwargs={'pk': proyecto.id, 'pku': us.id}))
        self.assertContains(response, 'Sin avances', msg_prefix="Error en team/test_modificar_daily")

    def test_modificar_daily_sin_permiso(self):
        """
       Setup inicial y luego se procede a intentar modifcar modificar un Daily perteneciente a un User Stor ya
       existente, se le brinda al usuario el permiso de modificar_proyecto sin embargo no el de modificar_daily por
       lo tanto la llamada post retorna un codigo http 403
       :return:
       """
        self.login()
        usuario = auth.get_user(self.client)

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        #assign_perm('Daily.modificar_daily', rol)



        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                           fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
        us = UserStory.objects.create(proyecto=proyecto, titulo='Login', descripcion_corta='Entrada al sistema',
                                      urgencia=3, valor_de_negocio=5, valor_tecnico=10, estado=1,
                                      tiempo_estimado_de_terminacion=2)

        d = Daily.objects.create(texto="Avance del 45%", us=us, autor='Jorge').id
        response = self.client.post(reverse('Proyecto:UserStory:Daily:ModificarDaily',
                                            kwargs={'pk': proyecto.id, 'pku': us.id, 'pkd': d}),
                                            data={'texto': 'Sin avances'})

        self.assertEqual(response.status_code, 403, msg="Error en Daily/test_modificar_daily_sin_permiso")