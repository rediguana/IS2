from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'Cliente'

urlpatterns = [
    path('cliente/add', views.ClienteCreate.as_view(), name='cliente-add'),
    path('cliente/list', views.ListaClientes.as_view(), name='cliente-list'),
    path('cliente/<int:pk>/modificar', views.ClienteUpdate.as_view(), name='cliente_modificar'),
    path('cliente/<int:pk>/eliminar', views.ClienteDelete.as_view(), name='cliente_eliminar'),
]
