from django import forms
from django.forms import BaseFormSet, inlineformset_factory

from Kanban.models import Flujo, Fase

"""Form para flujo, excluye el campo Proyecto"""
class FlujoForm(forms.ModelForm):
    nombre = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={'class': 'form-control',
                                      'placeholder': 'Nombre del flujo'
                                      }),
        required=True)
    class Meta:
        model = Flujo
        exclude = ('proyecto',)

"""inlineformset para relacionar multiples fases con un flujo"""
FaseFormSet = inlineformset_factory(Flujo,  # form padre
                                                  Fase,  # inline-form
                                                  fields=['fase', 'orden'], # inline-form campos
                                                  # help texts de los campos
                                                  help_texts={
                                                        'nombre':'nombre de la fase',
                                                        'orden': 'orden de la fase',
                                                  },
                                                  #puesto a falso porque no se puede eliminar fase que no exite
                                                  can_delete=False,
                                                  #cuantas in-line forms se muestran al inicio
                                                  extra=1)

"""inlineformset para relacionar multiples fases con un flujo"""
FaseModFormSet = inlineformset_factory(Flujo,  # form padre
                                                  Fase,  # inline-form
                                                  fields=['fase', 'orden'], # inline-form campos
                                                  # help texts de los campos
                                                  help_texts={
                                                        'nombre':'nombre de la fase',
                                                        'orden': 'orden de la fase',
                                                  },
                                                  #puesto a true para poder modificar
                                                  can_delete=True,
                                                  #cuantas in-line forms se muestran al inicio
                                                  extra=1)
