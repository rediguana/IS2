from django.test import TestCase
from django.urls import reverse
from .models import User, Rol
from guardian.shortcuts import assign_perm
from django.contrib import auth
from django.contrib.auth.models import Group, Permission, ContentType

from Proyecto.models import Proyecto

# Create your tests here.

class LoginViewTests(TestCase):

    def test_login_usuario_existente(self):
        """
        Este test crea un nuevo usuario e inicia sesión al sistema, se verifica la redirección a la página
        principal, el contenido de la página y el código de respuesta http.
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'

        usuario.save()

        response = self.client.post('/login/', data={'username':usuario.username, 'password':'123456'},follow=True)
        self.assertRedirects(response, '/')
        self.assertEqual(response.status_code, 200)




    def test_login_usuario_inexistente(self):
        """
        Se intenta iniciar sesión con un usuario que no existe y se verifica el código de respuesta http,
        que se muestre un mensaje de error y que el usuario no esté autenticado.
        """
        response =  response = self.client.post('/login/',data={
            'username': 'fulano',
            'password': '123'
        }, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_contraseña_incorrecta(self):
        """
        Se crea un nuevo usuario y se inicia sesión pero con una contraseña incorrecta, se verifica el código de
        respuesta http, que se muestre un mensaje de error y que el usuario no esté autenticado
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'

        usuario.save()

        response = self.client.post('/login/', data={'username': usuario.username, 'password': '654321'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['user'].is_authenticated)

    def test_login_usuario_inactivo(self):
        """
        Se crea un nuevo usuario y es puesto en un estado inactivo, se intenta iniciar sesión y se verifica el código de
        respuesta http, que se muestre un mensaje de error y que el usuario no esté autenticado.
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.is_active = False
        usuario.email= 'email@g.com'

        usuario.save()

        response = self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['user'].is_authenticated)



class RolCreateViewTests(TestCase):
    def setup(self):
        """
        Setup para algunos tests en donde se deben estar con un usuario valido conectado al cliente
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'

        usuario.save()
        #
        # content_type = ContentType.objects.get_for_model(Group)
        # permission = Permission.objects.create(codename='ver_roles', name='Puede acceder a lista de roles',
        #                                        content_type=content_type, )

        assign_perm('Accounts.ver_rol', usuario)
        assign_perm('Accounts.crear_rol',usuario)
        assign_perm('Accounts.modificar_rol',usuario)
        usuario.save()

        #
        # perm1 = Permission()
        # perm1.name = 'Crear usuario'
        # perm1.codename = 'crear_usuario'
        # perm1.content_type = ContentType.objects.get_for_model(User)
        #
        # perm1.save()
        #
        # perm2 = Permission()
        # perm2.name = 'Crear proyecto'
        # perm2.codename = 'crear_proyecto'
        # perm2.content_type = ContentType.objects.get_for_model(Proyecto)
        # perm2.save()


        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)


    def test_crear_nuevo_rol(self):
        """
        Setup inicial del test y luego se prueba crear un rol enviando una solicitud Post para crear un nuevo rol llamado
        SCRUM, el test es valido si se redirige a la lista de roles y el nuevo Rol se encuentra en la lista
        """

        self.setup()
        response = self.client.post(reverse('Accounts:crearRol'), data={'name': 'SCRUM', 'permissions' : '17'}, follow=True)
        self.assertRedirects(response,reverse('Accounts:listaRol'))
        self.assertContains(response,"SCRUM")


    def test_crear_rol_nombre_repetido(self):
        """
        Setup inicial del test y luego se prueba crear 2 roles con el mismo nombre y se verifica que el view se encargue
        de avisarte con un mensaje
        """
        self.setup()
        response = self.client.post(reverse('Accounts:crearRol'), data={'name': 'SCRUM', 'permissions': '17'},
                                    follow=True)
        self.assertRedirects(response, reverse('Accounts:listaRol'))
        self.assertContains(response, "SCRUM")

        response = self.client.post(reverse('Accounts:crearRol'), data={'name': 'SCRUM', 'permissions': '17'},
                                    follow=True)

        self.assertContains(response,"Group with this Name already exists.")


    def test_acceder_sin_permiso(self):
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'

        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        response = self.client.post(reverse('Accounts:crearRol'), data={'name': 'SCRUM', 'permissions': ['1', '2']},
                                    follow=True)

        self.assertEqual(response.status_code,403)

class RolListaViewTest(TestCase):

    def login(self):
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_acceder_sin_permiso(self):
        self.login()
        response = self.client.post(reverse('Accounts:listaRol'), data={'name': 'SCRUM', 'permissions': ['1', '2']},
                                    follow=True)

        self.assertEqual(response.status_code,403)

    # def test_lista_roles_existentes(self):
    #     self.login()
    #     usuario = auth.get_user(self.client)
    #     assign_perm('Accounts.ver_rol', usuario)
    #
    #     Group.objects.create(name='SCRUM')
    #     Group.objects.create(name='DEVELOPER')
    #
    #     response = self.client.get(reverse('Accounts:listaRol'),follow=True)
    #     self.assertContains(response, 'SCRUM')
    #     self.assertContains(response, 'DEVELOPER')

class RolModificarViewTest(TestCase):

    def login(self):
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'

        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_acceder_sin_permiso(self):
        """
        Test de acceder a la página sin el permiso correspondiente
        :return:
        """
        self.login()
        Group.objects.create(name='SCRUM')
        response = self.client.get(reverse('Accounts:modificarRol',kwargs={'pk': 1}), follow=True)

        self.assertEqual(response.status_code,403)

    def test_modificar_rol(self):
        """
        El test primero crea un rol, verifica que se encuentre en la lista, luego modifica el nombre del rol y verifica
        que se actualize el nombre en la lista
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Accounts.ver_rol', usuario)
        assign_perm('Accounts.modificar_rol', usuario)

        rol = Rol.objects.create(name='SCRUM')


        response = self.client.get(reverse('Accounts:listaRol'), follow=True)
        self.assertTemplateUsed(response, 'rol/rol_listar.html')

        response = self.client.post(reverse('Accounts:modificarRol',kwargs={'pk': rol.id}), data={'name':'DEVELOPER','permissions' : '17'},
                                    follow=True)

        self.assertTemplateUsed(response, 'rol/rol_listar.html')



class RolEliminarViewTest(TestCase):
    def login(self):
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email= 'email@g.com'

        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_eliminar_rol(self):
        """
        El test primero crea un rol, luego lo elimina y verifica que ya no aparezca el nombre en la lista
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Accounts.ver_rol', usuario)
        assign_perm('Accounts.eliminar_rol', usuario)
        id = Rol.objects.create(name='SCRUM').id


        response = self.client.post(reverse('Accounts:eliminarRol',kwargs={'pk': id}), follow=True)

        self.assertRedirects(response,reverse('Accounts:listaRol'))
        self.assertNotContains(response,'SCRUM')


    def test_eliminar_sin_permiso(self):
        """
        Test de acceder a la página sin el permiso correspondiente
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Accounts.ver_rol', usuario)
        id = Group.objects.create(name='SCRUM').id

        response = self.client.post(reverse('Accounts:eliminarRol',kwargs={'pk': id}), follow=True)
        self.assertEqual(response.status_code,403)
