from django.db import models

from django.contrib.auth.models import Group
from Accounts.models import User, Rol
from Proyecto.models import Proyecto
from auditlog.registry import auditlog
# Create your models here.

class team (models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True)
    horas_trabajadas = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    rol = models.ForeignKey(Rol, on_delete=models.DO_NOTHING, null=True, blank=True)

    def __str__(self):
        return str(self.user.username)

    class Meta:
        unique_together = ('proyecto', 'user',)
        default_permissions = []
        permissions = (
            ("listar_team", "Ver lista de team"),
            ("modificar_team", "Modificar teams"),
            ("crear_team", "Crear teams"),
            ("eliminar_team", "Eliminar teams"),
            ("ver_proyecto", "Ver proyecto")
        )

auditlog.register(team)