
from django.urls import path
from . import views
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from guardian.decorators import permission_required_or_403


# Urls del sistema
app_name = 'Team'
urlpatterns = [
    path('', views.Listateam.as_view(), name='listaTeam'),
    path('crear/', views.Crear_team.as_view(), name='crearTeam'),
    path('<int:pkt>/modificar/', views.Modificar_team.as_view(), name='modificarTeam'),
    path('<int:pkt>/eliminar/', views.Eliminarteam.as_view(), name='eliminarTeam')
    ]
