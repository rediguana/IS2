from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.forms import DateField

from IS2 import settings
from .models import team
from Accounts.models import  User


from django.contrib.auth.models import Group,Permission
from team.models import team
from Accounts.models import Rol, User
#Form de auntenticacion heredado de django
class TeamForm(forms.ModelForm):
    # def get_queryset(self):
    #     """
    #
    #     return   User.objects.all().exclude(team.objects.filter(id=self.kwargs['pkt']).iterator().get('user'))
    #
    user = forms.ModelChoiceField( queryset=User.objects.all().exclude(username='AnonymousUser'), widget=forms.Select() )

    horas_trabajadas = forms.DecimalField(label='horas_trabajadas', min_value=0.00,
                                 widget=forms.NumberInput(attrs={'class': 'form-control', 'name':'horas_trabajadas'}))

    rol = forms.ModelChoiceField(queryset=Rol.objects.all().exclude(isProyectoRol=False))

    class Meta:
        model = team
        fields = [
            'user',
            'horas_trabajadas',
            'rol',
        ]
